
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <div>
        <h3><a href="RegistrarLocal.php">Registrar un nuevo local</a></h3>
    </div>

    <table>
        <tr>
            <th>Id Local</th>
            <th>Direccion</th>
            <th>Telefono</th>
            <th>Encargado</th>
        </tr>
        <?php

                include '../../ArchivosPHP/Procesos.php';
                $Locales = new Procesos();
                $Campos = array(
                    "id_Local",
                    "Direccion",
                    "Telefono",
                    "Encargado"
                );
                $Datos = $Locales->Consulta($Campos,"locales_medicos");

                foreach($Datos as $fila){
                    echo "<tr>";
                    echo "<td>".$fila['id_Local']."</td>";
                    echo "<td>".$fila['Direccion']."</td>";
                    echo "<td>".$fila['Telefono']."</td>";
                    echo "<td>".$fila['Encargado']."</td>";
                    //valores en eliminar tabla = nombre de la tabla ejempo -> locales_medicos
                    // idCampo = el nombre del campo del id en la tabla ejemplo -> id_Local
                    // id= al campo en la tabla ejemplo -> id_local
                    echo "<td><a href='../Eliminar.php?tabla=locales_medicos&idCampo=id_Local&id=".$fila['id_Local']."'>Eliminar</a></td>";
                    echo "<td><a href='FormularioModificar.php?idD=".$fila['id_Local']."'>Modificar</a></td>";
                    
                    echo "</tr>";


                }

        ?>
        </table>
    
</body>
</html>


