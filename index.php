<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Document</title>

    <style>
        body{
            background-image:url(../img/fondo.jpg);
            background-size: 100% 100%;
            background-attachment: fixed;
        }
        
        form{
            margin:auto;
            text-align: center;
            margin-top:10%;
        }
        button{
            margin:auto;
        }
    </style>
</head>
<body>
    <form method="POST" class="col-md-4" action="ArchivosPHP/Login/sesion.php">

        <div class="form-group col-md-12">
            <label> Ingrese el nombre de usuario </label>
            <input type="text" name="UserN" class="form-control" required autocomplete="off">
        </div>
        <div class="form-group col-md-12">
            <label> Ingrese la contraseña </label>
            <input type="password" name="PassW" class="form-control" required autocomplete="off">
        </div>
       <button class="btn btn-lg btn-primary btn-block text-uppercase col-md-6" type="submit">Ingresar</button>
        <a href="Registrar.php">Registrar Usuario</a>
    </form>
    
    
</body>
</html>