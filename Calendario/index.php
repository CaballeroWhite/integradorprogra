<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Bootstrap-->
   
    <link rel="stylesheet" href="css/fullcalendar.min.css">
    
    <script src="js/jquery.min.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="js/fullcalendar.min.js"></script>

    <!--Colocar en espaniol-->
    <script src="js/es.js"></script>
    <!--Modal bootstrap jquery-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!--ClockPicker-->
    <script src="js/bootstrap-clockpicker.js"> </script>
    <link rel="stylesheet" href="css/bootstrap-clockpicker.css">
<!--  Barra de navegacion -->
    <link rel="stylesheet" href="../iconos/css/fontello.css">
    
    <link rel="stylesheet" href="../css/estilos.css">

    <style>
      .container{
        margin-top:5%;
      }
    </style>
    <title>Calendario</title>
</head>
<body>
 
<header>
        <div class="contenedor">
            <a href='index.html'><img src="IMG/log.png" width="auto" height="75"alt=""></a>
            <input type="checkbox" id='menu-bar'>
            <label class='icon-menu' for="menu-bar"></label>
            <h1 class='text-Center'>Control interno cruz roja</h1>
           
            <nav class='menu'>
                <a href="" class='icon-user-plus'>Registrar Usuario</a>
                <a href="" class='icon-user'>Control Usuarios</a>
                <a href="" class='icon-archive'>Control Inventario</a>
                <a href="" class='icon-address-book'>Control Voluntarios</a>
                <a href="../Calendario/index.php" class='icon-calendar'>Control Horarios</a>
                <a href="../ArchivosPHP/Login/CerrarSesion.php" class="icon-logout">Cerrar Sesion</a>
                
                
                
            </nav>
           
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col-9"><br><br><div id="CalendarioWeb"></div></div>
            <div class="col"></div>
        </div>
    </div>
    
    
    <script>
        $(document).ready(function(){
          $('#CalendarioWeb').fullCalendar({
            header:{
              left:'today,prev,next',
              center:'title',
              right:'month,agendaWeek,agendaDay'
            },
            dayClick:function(date, jsEvent, view){
              //Bloquear botones
              $('#agregar').prop("disabled",false);//False para mostrar
              $('#modify').prop("disabled",true); //True para evitar dar clic
              $('#eliminar').prop("disabled",true);


              limpiarFormulario();
              $('#txtFecha').val(date.format());
              $('#ModalEventos').modal();
            },
            events:'http://localhost/integradorprogra/Calendario/eventos.php',
            eventClick: function(calEvent, jsEvent, view) {
                  //Bloquear botones
                  $('#agregar').prop("disabled",true);//False para mostrar
                  $('#modify').prop("disabled",false); //True para evitar dar clic
                  $('#eliminar').prop("disabled",false);
                  //mostrar informacion en input
                  $('#TituloEvento').html(calEvent.title);
                  $('#id').val(calEvent.id);
                  $('#txVoluntario').val(calEvent.NombreVoluntario);
                  $('#txDescripcion').val(calEvent.descripcion);
                  $('#txTitulo').val(calEvent.title);
                  $('#txColor').val(calEvent.color);

                  FechaHora = calEvent.start._i.split(" ");//Con esto sepera la fecga de la hora start
                  FechaHoraSalida = calEvent.end._i.split(" ");
                  $('#txtFecha').val(FechaHora[0]);
                  $('#txHora').val(FechaHora[1]);
                  
                  $('#txtFecha').val(FechaHoraSalida[0]);
                  $('#txHoraSalida').val(FechaHoraSalida[1]);


                  $('#ModalEventos').modal();
              },
              editable:true,
              eventDrop:function(calEvent){
                $('#id').val(calEvent.id);
                $('#txTitulo').val(calEvent.title);
                $('#txColor').val(calEvent.color);
                $('#txDescripcion').val(calEvent.descripcion);
                $('#txVoluntario').val(calEvent.NombreVoluntario);

                var fechaHora = calEvent.start.format().split("T");
                
                
                $('#txtFecha').val(fechaHora[0]);
                $('#txHora').val(fechaHora[1]);

                var FechaHoraSalida = calEvent.end.format().split("T");
                
                
                $('#txHoraSalida').val(FechaHoraSalida[1]);
                

                ObtenerDatosEvento();
                EnviarInformacion('modificar',NuevoEvento,true);


              }
          });
        });
      
    </script>
   <!-- ModalEventos -->
<div class="modal fade" id="ModalEventos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloEvento"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!--Ocultos al usuario-->
          <input type="hidden" id="id" name="id">
          <input type="hidden" id="txtFecha" name="fecha">

        <div class="form-row">
              <div class="form-group col-md-3">
                  <label for="">Titulo</label>
                  <input type="text" class='form-control' id="txTitulo">
              </div>

              <div class="form-group col-md-5">
                <label for="">Nombre del Voluntario</label>
                <input type="text" class='form-control' id="txVoluntario">
            </div>
              <div class="form-group col-md-4">
                  <label for="">Hora de Entrada</label>
                  <div class="input-group clockpicker" data-autoclose='true'>
                      <input type="text" class='form-control' id="txHora" autocomplete="off" value="">

                  </div>
                  
              </div>

              <div class="form-group col-md-4">
                <label for="">Hora de Salida</label>
                <div class="input-group clockpicker" data-autoclose='true'>
                    <input type="text" class='form-control' id="txHoraSalida" autocomplete="off" value="">

                </div>
                
            </div>
                  
        </div>
        <div class="form-group">
            <label for="">descripcion</label>
            <textarea id="txDescripcion" class='form-control' rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="">color</label>
            <input type="color" class='form-control' value="#FF0000" id="txColor">
        </div>   
              
                  
              
              
                    

                  

        
          

      </div>
      <div class="modal-footer">
        
              <button type="button" id='agregar'class="btn btn-success">Guardar</button>
              <button type="button" id='modify' class="btn btn-primary">Modificar</button>
              <button type="button" id='eliminar' class="btn btn-danger">Borrar</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>

<script>
  var NuevoEvento;
  $('#agregar').click(function(){
    ObtenerDatosEvento();
    
    EnviarInformacion('agregar',NuevoEvento);
  });

  $('#eliminar').click(function(){
    ObtenerDatosEvento();
    EnviarInformacion('eliminar',NuevoEvento);
  });
  $('#modify').click(function(){
    ObtenerDatosEvento();
    EnviarInformacion('modificar',NuevoEvento);
  });

  function ObtenerDatosEvento(){
    //Tiene que ser igual en eventos al insertar
      NuevoEvento = {
        id:$('#id').val(),
        title:$('#txTitulo').val(),
        NombreVoluntario:$('#txVoluntario').val(),
        start:$('#txtFecha').val()+" "+$('#txHora').val(),
        color:$('#txColor').val(),
        descripcion:$('#txDescripcion').val(),
        textColor:'#fff',
        end:$('#txtFecha').val()+" "+$('#txHoraSalida').val()
      };
  }

  function EnviarInformacion(accion,objEvento,modal){
      $.ajax({
          type:'POST', //Ajax para eviar por POST
          url:'Eventos.php?accion='+accion,//Es decir agregar modificar o elimina
          data:objEvento, //La informacion del evento
          success:function(msg){ //Con esto se obtiene una respuesta es decir verdadero o falso de la insercion
            if(msg){
              $('#CalendarioWeb').fullCalendar('refetchEvents');//Actualiza eventos
              if(!modal){
                $('#ModalEventos').modal('toggle');
              }
              
            }
          },

          error:function(){
              alert("Error");
          }

      })
  }

  $(".clockpicker").clockpicker();

  function limpiarFormulario(){
                  $('#id').val('');
                  $('#txDescripcion').val('');
                  $('#txTitulo').val('');
                  $('#txColor').val('');

  }
</script>  


 
</body>
</html>