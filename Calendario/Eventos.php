<?php
header('Content-Type: application/json');
include '../ArchivosPHP/Conexion.php';
$Conexion = new conexionPDO();
$PDO = $Conexion->Conectar();

$accion = (isset($_GET['accion']))?$_GET['accion']:'leer'; //Condicional ternario
$SentenciaSql;
switch($accion){
    case 'agregar':
    //Instruccion Agregar
        $SentenciaSql = $PDO->prepare(
            "INSERT INTO horarios (title,NombreVoluntario,descripcion,color,textColor,start,end) VALUES(:title,:NVoluntario,:descripcion,:color,:textColor,:start,:end)"
        );
        $InsertarDatos = $SentenciaSql->execute(
             //Tiene que ser igual la funcionObtenerDatos del evento click
            array(
                "title"=>$_POST['title'],
                "NVoluntario"=>$_POST['NombreVoluntario'],
                "descripcion"=>$_POST['descripcion'],
                "color"=>$_POST['color'],
                "textColor"=>$_POST['textColor'],
                "start"=>$_POST['start'],
                "end"=>$_POST['end'],
            )
        );
        echo json_encode($InsertarDatos);//respuesta true o false
        break;
    case 'eliminar':
     //Instruccion eliminar
        $respuesta = false;
        if(isset($_POST['id'])){
            $SentenciaSql = $PDO->prepare(
                "DELETE FROM horarios WHERE id=:ID"
            );
            $respuesta=$SentenciaSql->execute(array("ID"=>$_POST['id']));
            
        }
        echo json_encode($respuesta);
        
        break;
    case 'modificar':
         //Instruccion update

         $SentenciaSql = $PDO->prepare("UPDATE horarios SET
         title=:title,
         NombreVoluntario=:NVoluntario,
         descripcion=:descripcion,
         color=:color,
         textColor=:textColor,
         start=:start, /*Hra inicio */
         end=:end /* hra salida*/
         WHERE id=:ID
         "
         );
        $respuesta = $SentenciaSql->execute(
            //Tiene que ser igual la funcionObtenerDatos del evento click
            array(
                "ID"=>$_POST['id'],
                "title"=>$_POST['title'],
                "NVoluntario"=>$_POST['NombreVoluntario'],
                "descripcion"=>$_POST['descripcion'],
                "color"=>$_POST['color'],
                "textColor"=>$_POST['textColor'],
                "start"=>$_POST['start'],
                "end"=>$_POST['end']
            )
        );
        echo json_encode($respuesta);


        break;
    
    default:
            $stm = $PDO->prepare("SELECT * FROM horarios");
            $stm->execute();

            $eventos = $stm->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($eventos);
        break;
    
}


//Luego hare una clase llamada evento con un metodo recuperar eventos




?>