<?php

include 'Conexion.php';

  class Procesos{
    public $conectarv;
    
    public function __construct()
    {
        $this->conectarv = new conexionPDO();
    }

   
    //Metodo que recibe los campos de una tabla a mostrar
    //campoT recibe un array
    public function Consulta($camposT,$tabla){
        $arreglo=null;
        $conexion = $this->conectarv->Conectar();
        $campos = implode(",",$camposT); //Convertir array a string separados por ,
        $SQL = "SELECT $campos FROM $tabla";
        $statement = $conexion->prepare($SQL);
        $statement->execute();

        while($resultado = $statement->fetch()){    
            $arreglo[]=$resultado;
        }
        
        return  $arreglo;
    }

    //Metodo que recibe los campos de una tabla a mostrar
    //campoT recibe un array
    public function CargaDatos($id,$NombreIdCampo,$tabla){
        $arreglo=null;
        $conexion = $this->conectarv->Conectar();
       
        $SQL = "SELECT * FROM $tabla WHERE $NombreIdCampo = :id";
        $statement = $conexion->prepare($SQL);
        $statement->bindParam(":id",$id);
        $statement->execute();

        while($resultado = $statement->fetch()){    
            $arreglo[]=$resultado;
        }
        
        return  $arreglo;
    }

    public function Insertar($camposT,$tabla,$valoresArray){

        $conexion = $this->conectarv->Conectar();
        $campos = implode(",",$camposT); //Convertir array a string separados por ,
        $Arg = array_fill(0,count($valoresArray),'?'); //Crea un array repitiendo el signo ?
        $valoresInsertar = implode(',',$Arg);//Convierte la variable $ARG que es un array a string separados por ,
        
        $SQL = "INSERT into $tabla($campos) VALUES($valoresInsertar)";
        $statement = $conexion->prepare($SQL);
        
        //echo $valoresInsertar;
        if(!$statement){
            return "Error en el registro";
        }else{
            
            $statement->execute($valoresArray); //Inserta el array

            return "Datos registrados";
        }
        
    }

    //La variable NombreIdCampo corresponde al id de la tabla en la cual se esta trabajando
    //La variable IdEliminar corresponde al id que se recibira
    public function Eliminar($tabla,$NombreIdCampo,$IdEliminar){
        $conexion = $this->conectarv->Conectar();
        $SQL = "DELETE FROM $tabla WHERE $NombreIdCampo = :id";
        $statement= $conexion->prepare($SQL);
        $statement->bindParam(':id',$IdEliminar);

        if(!$statement){
            return 'Error al eliminar';
        }else {
            $statement->execute();
            return 'Registro eliminado';
            
            
        }

    }

    //La variable NombreCampo es para indicar cual sera la columna actualizada
    //Variable NuevoValor para asiganar el nuevo dato
    //La variable NombreIdCampo corresponde al id de la tabla en la cual se esta trabajando
    //La variable id, es el que recibira para actualizar el registro
    public function Modificar($tabla,$NombreCampo,$NuevoValor,$NombreIdCampo,$Id){

        $conexion = $this->conectarv->Conectar();
        $SQL = "UPDATE $tabla SET $NombreCampo = :NuevoValor WHERE $NombreIdCampo = :id";
        $statement = $conexion->prepare($SQL);
        $statement->bindParam(':NuevoValor',$NuevoValor);
        $statement->bindParam(':id',$Id);

        if(!$statement){
            return "Error al modificar";
        }else{
            $statement->execute();
            return "Registro modificado";
        }



    }




  }

  //$Docente = new Procesos();
/*
  $campos = array(
      'Nombre',
      'Apellido',
      'Direccion'
      
  );

 
  $Valores =array(
    'fdf','xxx','rty'   
  );
      

  
  $filas = $Docente->Consulta($campos,"docentes1");
  
  $Docente->Insertar($campos,"docentes1",$Valores);*/

  //$Docente->Eliminar("docentes1","idD",7);

  //$Docente->Modificar("docentes1","Nombre","Juanito","idD",2);
  












?>