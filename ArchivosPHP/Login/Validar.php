<?php


require '../Conexion.php';

class ValidarSesion{
    public $conectarv;

    public function __construct()
    {
        $this->conectarv = new conexionPDO();
    }

    public function Login(){
        
        $conexion = $this->conectarv->Conectar();
        $SQL = 'SELECT * FROM Usuario WHERE UserName=:UsuarioN AND PassUser = :PassUsuario';
        $sth = $conexion->prepare($SQL);

        $sth->bindParam(":UsuarioN",$_POST['UserN']);
        $sth->bindParam(":PassUsuario",$_POST['PassW']);
        $sth->execute();

        $UsuarioSesion = $sth->fetchAll();

        if($UsuarioSesion){
            session_start();//Iniciar la sesion

            $tipoUsuario = $UsuarioSesion[0]['tipo'];

            switch($tipoUsuario){
                case "Admin":
                        $_SESSION['user'] = $UsuarioSesion[0]['UserName'];
                        $_SESSION['tipo'] = $UsuarioSesion[0]['tipo'];
                        //Menu del administrador
                        header('location:../../MenuUsuarios/menuAdmin.php');
                        echo "inicio de sesion como admin";
                        break;
                case "Estandar":
                        $_SESSION['user'] = $UsuarioSesion[0]['UserName'];
                        $_SESSION['tipo'] = $UsuarioSesion[0]['tipo'];
                        //Menu del usuario estandar
                        header('location:../../MenuUsuarios/menuEstandar.php');
                        echo "inicio de sesion como estandar";
                        break;

            }

        }else{
            //Manda a iniciar sesion
            header('location:../../index.php');
        }

        
    }



}


?>